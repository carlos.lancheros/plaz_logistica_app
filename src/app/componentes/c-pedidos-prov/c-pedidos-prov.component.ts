import { DataService } from './../../servicios/data.service';
import { Component, OnInit, Input } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { LoadingService } from '../../servicios/loading.service';
import { AlertService } from '../../servicios/alert.service';


@Component({
  selector: 'app-c-pedidos-prov',
  templateUrl: './c-pedidos-prov.component.html',
  styleUrls: ['./c-pedidos-prov.component.scss'],
})
export class CPedidosProvComponent implements OnInit {

  @Input() idPed: string;
  @Input() idProv: string;
  @Input() tipP: string;
  infoPedProv: any;
  habilitado = false;

  constructor(private dataService: DataService,
              private alertCtrl: AlertController,
              private alertCtrlS: AlertService,
              private loadingCtrl: LoadingService,
              private navCtrl: NavController) { }

  ngOnInit() {
    this.dataService.getInfoPedProv(this.idPed, this.idProv)
      .subscribe((data) => {
        this.infoPedProv = data;
        if (data[0] != null) {
          if (data[0].ped_estado === 'ALIS' && this.tipP !== 'OPER') {
            this.habilitado = true;
          } else {
            this.habilitado = false;
          }
        }
      });
  }

  onFinAlist() {
    this.prAlertaConfirmacion();
  }

  cmbEst( estBol: boolean ) {
    let numEst;
    switch (estBol) {
      case true:
        numEst = 1;
        break;
      case false:
        numEst = 0;
        break;
    }
    return numEst;
  }

  actProd( check ) {
    this.dataService.putEstadoProducto(this.idPed, this.idProv, check.id, this.cmbEst(check.estado))
      .subscribe(data => {
    }, err => console.log('Error Actualizando El check!!'));
  }

  async prAlertaConfirmacion() {
    const alert = await this.alertCtrl.create({
      header: '¡Confirmar Alistamiento!',
      message: '¿Desea confirmar el alistamiento del pedido <strong>' + this.idPed + '</strong>?',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.loadingCtrl.prLoading('Confirmando Entrega.');
            this.dataService.putEstadoPedido(this.idPed, 'TRAN', '')
            .subscribe(data => {
              this.loadingCtrl.closeLoading();
              // tslint:disable-next-line: no-string-literal
              if (data['affectedRows'] !== 0) {
                this.navCtrl.navigateForward('/proveedor/' + this.idProv + '/PROV');
              } else {
                this.alertCtrlS.alertaError('No se ha podido actualizar la confirmacion del pedido.');
              }
            }, err => this.loadingCtrl.closeLoading() &&
                      this.alertCtrlS.alertaError('No se ha podido actualizar la confirmacion del pedido.')
            );
          }
        }
      ]
    });

    await alert.present();
  }
}
