import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CIngresarComponent } from './c-ingresar/c-ingresar.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CHeaderComponent } from './c-header/c-header.component';
import { CPedidosTranComponent } from './c-pedidos-tran/c-pedidos-tran.component';
import { CPedidosProvComponent } from './c-pedidos-prov/c-pedidos-prov.component';


@NgModule({
  declarations: [
    CIngresarComponent,
    CHeaderComponent,
    CPedidosTranComponent,
    CPedidosProvComponent,
  ],
  exports: [
    CIngresarComponent,
    CHeaderComponent,
    CPedidosTranComponent,
    CPedidosProvComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class ComponentesModule { }
