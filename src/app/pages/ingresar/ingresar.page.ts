import { Component, OnInit } from '@angular/core';
import { NavController, Platform, AlertController  } from '@ionic/angular';
import { DataService } from '../../servicios/data.service';
import { AlertService } from '../../servicios/alert.service';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.page.html',
  styleUrls: ['./ingresar.page.scss'],
})

export class IngresarPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  ngOnInit() { }

  evAtras() {
    this.navCtrl.navigateBack('inicio');
  }
}
