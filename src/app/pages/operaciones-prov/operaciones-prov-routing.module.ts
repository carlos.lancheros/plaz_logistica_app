import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OperacionesProvPage } from './operaciones-prov.page';

const routes: Routes = [
  {
    path: '',
    component: OperacionesProvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OperacionesProvPageRoutingModule {}
