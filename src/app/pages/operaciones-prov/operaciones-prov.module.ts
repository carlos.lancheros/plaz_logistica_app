import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OperacionesProvPageRoutingModule } from './operaciones-prov-routing.module';

import { OperacionesProvPage } from './operaciones-prov.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OperacionesProvPageRoutingModule
  ],
  declarations: [OperacionesProvPage]
})
export class OperacionesProvPageModule {}
