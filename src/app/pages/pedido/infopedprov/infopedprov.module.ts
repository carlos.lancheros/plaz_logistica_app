import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfopedprovPageRoutingModule } from './infopedprov-routing.module';

import { InfopedprovPage } from './infopedprov.page';
import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfopedprovPageRoutingModule,
    ComponentesModule
  ],
  declarations: [InfopedprovPage]
})
export class InfopedprovPageModule {}
