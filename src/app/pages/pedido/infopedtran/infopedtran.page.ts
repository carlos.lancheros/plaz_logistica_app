import { DataService } from './../../../servicios/data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { LoadingService } from '../../../servicios/loading.service';
import { AlertService } from '../../../servicios/alert.service';


@Component({
  selector: 'app-infopedtran',
  templateUrl: './infopedtran.page.html',
  styleUrls: ['./infopedtran.page.scss'],
})
export class InfopedtranPage implements OnInit {

  idPed: string;
  tipPer: string;
  strDefHref: string;
  constructor(private rutaP: ActivatedRoute) { }

  ngOnInit() {
    this.idPed = this.rutaP.snapshot.paramMap.get('idPed');
    this.tipPer = this.rutaP.snapshot.paramMap.get('tipP');
    this.strDefHref =  (this.tipPer === 'TRAN') ? 'transportadores/TRAN' : 'transportadores/OPER';
  }
}
