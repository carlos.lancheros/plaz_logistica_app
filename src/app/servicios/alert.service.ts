import { AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AlertService {
  constructor(private alertCtrl: AlertController) { }

  async alertaError(msjAlert: string) {
    const alert = await this.alertCtrl.create({
      header: '¡Error!',
      subHeader: '',
      message: msjAlert,
      buttons: ['OK']
    });
    await alert.present();
  }
}
