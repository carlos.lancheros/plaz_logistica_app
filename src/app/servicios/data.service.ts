import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  constructor(private http: HttpClient) { }
  getUsuario(prmUsu: string, prmCtr: string) {
    return this.http.get('http://localhost:3000/LoginAppLogistica?prmUsu='  + prmUsu + '&prmCtr=' + prmCtr);
  }
  getPedProveedor(idProv: string) {
    return this.http.get('http://localhost:3000/PedidosProveedor?idPrv=' + idProv);
  }
  getPedTransportadores() {
    return this.http.get('http://localhost:3000/PedidosTransportadores');
  }
  getProveedores() {
    return this.http.get('http://localhost:3000/Proveedores');
  }
  getInfoPedTrans(idPed: string) {
    return this.http.get('http://localhost:3000/InfoXpedTrans?idPed=' + idPed);
  }
  getInfoPedProv(idPed: string, idProv: string){
    return this.http.get('http://localhost:3000/InfoXpedProv?idPed=' + idPed + '&idProv=' + idProv);
  }
  putEstadoPedido(idPed: string, strEst: string, strCm: string) {
    return this.http.get('http://localhost:3000/ActualizaEstPedido?idPed=' + idPed + '&strEst=' + strEst + '&strCom=' + strCm);
  }
  putEstadoProducto(idPed: string, idPrv: string, idProd: string, estPrm: number) {
    return this.http.get('http://localhost:3000/ActualizaEstProducto?idPed=' + idPed + '&idPrv='
                        + idPrv + '&idProd=' + idProd + '&estPrm=' + estPrm);
  }
}
